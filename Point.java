public class Point extends Sujet {
    protected int x, y;

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
        notifieObservateurs("X", this.x);
    }

    public void setY(int y) {
        this.y = y;
        notifieObservateurs("Y", this.y);
    }

}
