
public class ObservateurCoordonnee implements Observateur {
    private String modification = "PAS DE MODIFICATION";
    private String nom;
    
    public ObservateurCoordonnee(String nom){
        this.nom = nom;
    }

    @Override
    public void metsAJour(String attributModifie,
    Object valeur) {
        modification = "Attribut : " + attributModifie
        + " Valeur : " + valeur;
    }

    public String getModification() {
        return modification;
    }
    
    public String getNom(){
        return this.nom;
    }

}
