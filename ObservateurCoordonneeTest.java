
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Random;

/**
 * Classe-test ObservateurCoordonneeTest.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 *
 * Les classes-test sont documentées ici :
 * http://junit.sourceforge.net/javadoc/junit/framework/TestCase.html
 * et sont basées sur le document Š 2002 Robert A. Ballance intitulé
 * "JUnit: Unit Testing Framework".
 *
 * Les objets Test (et TestSuite) sont associés aux classes à tester
 * par la simple relation yyyTest (e.g. qu'un Test de la classe Name.java
 * se nommera NameTest.java); les deux se retrouvent dans le męme paquetage.
 * Les "engagements" (anglais : "fixture") forment un ensemble de conditions
 * qui sont vraies pour chaque méthode Test à exécuter.  Il peut y avoir
 * plus d'une méthode Test dans une classe Test; leur ensemble forme un
 * objet TestSuite.
 * BlueJ découvrira automatiquement (par introspection) les méthodes
 * Test de votre classe Test et générera la TestSuite conséquente.
 * Chaque appel d'une méthode Test sera précédé d'un appel de setUp(),
 * qui réalise les engagements, et suivi d'un appel à tearDown(), qui les
 * détruit.
 */
public class ObservateurCoordonneeTest
{
    // Définissez ici les variables d'instance nécessaires à vos engagements;
    // Vous pouvez également les saisir automatiquement du présentoir
    // à l'aide du menu contextuel "Présentoir --> Engagements".
    // Notez cependant que ce dernier ne peut saisir les objets primitifs
    // du présentoir (les objets sans constructeur, comme int, float, etc.).
    protected Observateur observateur;
    protected String attribut;
    protected int value;
    protected String modif;
    protected Random rnd;

    /**
     * Constructeur de la classe-test ObservateurCoordonneeTest
     */
    public ObservateurCoordonneeTest()
    {
    }

    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
        // Initialisez ici vos engagements
        observateur= new ObservateurCoordonnee("obsUn");
        rnd = new Random();
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }

    @Test
    public void testXModification()
    {
        attribut = "x";
        value = rnd.nextInt(100);
        modif = String.format("Attribut : %s Valeur : %d", attribut, value);
        observateur.metsAJour(attribut, value);
        assertEquals(observateur.getModification(), modif);
    }

    @Test
    public void testYModification()
    {
        attribut = "y";
        value = rnd.nextInt(100);
        modif = String.format("Attribut : %s Valeur : %d", attribut, value);
        observateur.metsAJour(attribut, value);
        assertEquals(observateur.getModification(), modif);
    }

    @Test
    public void testXYModification()
    {   
        String attrs[] = {"x", "y"};
        for (String attribut : attrs) {
            value = rnd.nextInt(100);
            modif = String.format("Attribut : %s Valeur : %d", attribut, value);
            observateur.metsAJour(attribut, value);
            assertEquals(observateur.getModification(), modif);
        }
    }
    
    @Test
    public void testNom(){
        ObservateurCoordonnee obs1 = new ObservateurCoordonnee("obsUn");
        assertEquals(obs1.getNom(), "obsUn");
    }
}

