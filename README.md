# Projet: votation

**Durée du devoir surveillé** : 4 heures

**Documentation** : supports de cours fournis sur Arche, TPs précédents, documentations Java Oracle (http://docs.oracle.com/javase/8/docs/api/index.html).

## Introduction

L'objectif de ce projet est de faire un système de validation de modification
d'objets par des *électeurs*, qui ont en charge d'accepter ou de refuser une
modification sur un ou plusieurs objets qu'ils suivent.
Un développeur doit pouvoir ajouter facilement des nouveaux *électeurs*,
il doit également pouvoir modifier la *stratégie* de validation combinant
l'ensemble des votes des électeurs.

## Consignes

Vous avez le droit de consulter vos précédents programmes, la documentation
Java sur Internet, mais n'avez pas le droit de vous faire aider par des
personnes autre que votre enseignant, qu'elles soient près de vous ou à distance.

Toute tentative de fraude sera sanctionnée par la note 0.

En cas de problèmes, si vous voulez récupérer un fichier enregistrez dans
un `commit` précédent utilisez la commande : `git checkout monfichier.java`

Chaque partie peut être réalisée en 1 heure environ.

En raison du caractère exceptionnel des conditions de réalisation de ce devoir individuel
vous devrez transmettre votre travail dans votre dépôt en ligne `Bitbucket`toutes les demi-heures.
Je vous rappelle que la transmission se fait par la commande : `git push --all` après un avoir effectué un `commit` pour valider l'état actuel.

Voici une méthode pratique pour vous assurer de faire un dépôt toutes les demi-heures :

* traitez une question à la fois ;
* enregistrez votre travail avec la commande `git commit -am "résumé du travail"` ;
* immédiatement après le `commit` envoyez votre programme : `git push --all` ;
* traitez la question suivante.

Bon courage !

- - -

# Préparation du projet (1 point)

1. Créer une bifurcation (fork) du projet **votation** dans votre compte Bitbucket.
2. Ajouter `lcpierron` ou `Laurent.Pierron@inria.fr` en administrateur du projet.
3. Cloner votre projet sur votre machine locale.

Le programme peut être testé en utilisant la méthode `main()` de la classe `Test`.
Ce programme interactif permet de modifier un attribut 'x' ou 'y' d'un point
défini dans le programme.
Le point est suivi par plusieurs observateurs de coordonnées,
la modification du premier observateur est affichée après la modification.
Les tests unitaires permettent de s'assurer du bon comportement des objets
des classes `ObservateurCoordonnee` et `Point`.
Dans la version initiale du programme la classe `Point` ne se comporte pas comme attendu.

Trace de l'exécution initiale de la méthode `main()`, il y a bien entendu une erreur car l'observation devrait afficher l'attribut et la vauleur modifiées :

    Point: (10, 15)
    Tapez :
    x pour modifier X
    y pour modifier Y
    q pour quitter
    x
    Entrez la coordonnée X : 13
    Point: (13, 15)
    Observation : PAS DE MODIFICATION
    Tapez :
    x pour modifier X
    y pour modifier Y
    q pour quitter
    y
    Entrez la coordonnée Y : 26
    Point: (13, 26)
    Observation : PAS DE MODIFICATION
    Tapez :
    x pour modifier X
    y pour modifier Y
    q pour quitter
    q

- - -

# Premiere partie - mettre en oeuvre le *patron de conception* **observateur** (6 points)

La troisième question sur l'ajout d'un nom aux observateurs est indépendante
des deux premières.

## Compléter la méthode `notifieObservateurs` de la classe `Sujet`  (2 points)

Ecrivez le contenu de la méthode `notifieObservateurs` de la clase `Sujet`afin qu'elle
mette à jour tous les objets `Observateur` contenus dans l'attribut `observateurs`.

Vous pouvez vous aider des tests définis dans la classe `ObservateurCoordonneTest`
pour voir quelle méthode notifie les objets `Observateur`.

Pour que tous les test passent et que les `observateurs` soient bien informés, il faut également modifier les modifieurs de la classe `Point` qui implémente l'interface `Observateur`. C'est le sujet de la question suivante.

## Compléter les modifieurs de la classe `Point`  (1 points)

Compléter les méthodes de modification de la classe `Point`, pour que
les observateurs soient notifiés.

Tous les tests unitaires doivent passer et permettent de valider les deux
questions.

Le programme de `Test` doit également fonctionner et afficher les modifications
du premier observateur.

Si ce n'est déjà fait, il est temps de *committer* votre travail avec un message
explicite.
Vous devez également synchronisez votre code sur Bitbucket
avec la commande : `git push --all`

## Ajout d'un attribut `nom` aux objets `ObservateurCoordonnee`  (3 points)

Afin d'identifier les observateurs, on souhaite leur ajouter un attribut `nom`
de type `String`.

1. Ajoutez un attribut `nom` dans `ObservateurCoordonnee` et/ou `Observateur`.
1. Ajoutez un constructeur prenant cet attribut en paramètre.
1. Modifiez les programmes qui ne compilent plus.
1. Ajoutez une méthode `public String getNom()`, qui renvoie la valeur du `nom`.
1. Ajoutez une méthode de test `testNom()` à la classe de test `ObservateurCoordonneeTest`.
1. Dans le programme `main()` de la classe `Test` affichez le nom de l'observateur avant la valeur
   de la modification.

Testez votre programme.

N'oubliez pas de *committer* votre travail après chaque modification avec un message
explicite.

À la fin de cette partie, même si vous n'avez pas tout fait et y reviendrez plus
tard, synchronisez votre code sur Bitbucket : `git push --all`

- - -

# Seconde partie - patron de conception **Voteur** (6 points)

Dans cette partie on souhaite modifier les observateurs en électeurs,
qui quand ils reçoivent une notification de modification,
ils décident chacun si ils acceptent ou refusent la modification.
Quand un électeur a refusé une modification, le `Point` doit retrouver
sa valeur initiale et tous les observateurs doivent en être notifiés.

Donc la méthode `metsAJour()` de la classe `Observateur`, renvoie une valeur
booléenne, qui indique si la modification est acceptée.

La classe `Observateur` a été transformée en `Electeur` et la classe
`ObservateurCoordonnee` en `ElecteurCoordonnee`.

Basculer dans la branche `voteur`, qui contient déjà les classes préparées
pour cette nouvelle fonctionnalité :

```bash
git checkout voteur
```

# Modifier `notifieElecteurs` de la classe `Sujet` (2 points)

La méthode `boolean notifieElecteurs(String, Object)` renvoie maintenant un booléen
qui indique si la modification a été acceptée par tous les électeurs.
Ce retour devra être utilisé par l'objet de type `Point` appelant, qui
ne devra pas être modifié si `notifieElecteurs` a retourné `false`.

Ecrire le contenu de la méthode `notifieElecteurs`. Comme pour la première partie
elle ne peut être validée que si les modifieurs de la classe `Point` ont été
mis à jour.

# Compléter les modifieurs de la classe `Point`  (3 points)

L'algorithme utilisé pour les modifieurs de la classe `Point` est le suivant :

1. Modifier l'objet et notifier les électeurs.
1. Si un électeur a refusé la modification :
   1. Remettre l'objet dans l'état initial
   1. Notifier les électeurs du retour à l'état initial

On suppose que l'état initial de l'objet de type `Point` a été accepté
par tous les électeurs.

Testez votre programme, notamment les tests unitaires doivent passer
et le programme `Test` doit fonctionner correctement.

N'oubliez pas de *committer* votre travail et de le synchroniser sur `Bitbucket`.

# Ajouter une méthode `public String toString()` à la classe `ElecteurCoordonnee` (1 point)

Cette méthode doit permettre d'afficher l'attribut pris en compte par
l'électeur et les limites de cet attribut.

Vous pouvez tester dans le programme `Test`, que l'affichage des électeurs
est correct.

N'oubliez pas de *committer* votre travail après chaque modification avec un message
explicite.

À la fin de cette partie, même si vous n'avez pas tout fait et y reviendrez plus
tard, synchronisez votre code sur Bitbucket : `git push --all`

- - -

# Troisième partie - patron de conception **Stratégie** (6 points)

En préalable, basculer dans la branche `strategie`, qui contient déjà les classes préparées
pour cette nouvelle fonctionnalité :

```bash
git checkout strategie
```

Maintenant on souhaite modifier le programme pour que les électeurs puissent avoir
plus de deux choix de vote, en votant de la sorte :

* POUR : l'attribut surveillé est dans les limites autorisées ;
* CONTRE : l'attribut surveillé est hors des limites autorisées ;
* NEUTRE : la modification ne concerne pas l'attribut modifié.

Les différents votes sont représentés par l'énumération `Vote`.
Cette énumération sert maintenant de retour à la méthode `metsAJour()`
de l'interface `Electeur`.

On souhaite pouvoir choisir entre plusieurs stratégies de vote, dans
la première version la validation de la modification se faisait
à l'unanimité, dans cette nouvelle version on veut pouvoir choisir
la stratégie.

Pour faire cela on va mettre en oeuvre le patron de conception **Stratégie**,
qui consiste à déléguer un traitement à une classe annexe qui sera en paramètre
de la classe principale utilisatrice de la stratégie.

Les différentes stratégies partageront une interface commune `StrategieVote`,
qui demandera la réalisation des méthodes suivantes :

* `void initialiseVotes()` : initialisation de la stratégie
* `void ajouteVote(Vote vote)` : ajout d'un vote à la stratégie
* `boolean getResultat()` : le résultat de la stratégie sous forme de booléen,
  la modification est acceptée si la valeur retournée est vraie.



# Modifier `notifieElecteurs` de la classe `Sujet` (2 points)

La méthode `boolean notifieElecteurs(String, Object)` doit maintenant faire
appel à la stratégie `strategieVote` pour valider la modification.

Ecrire le contenu de la méthode `notifieElecteurs`. Comme pour la première partie
elle ne peut être validée que si les modifieurs de la classe `Point` ont été
mis à jour.

# Compléter les modifieurs de la classe `Point`  (1 point)

L'algorithme utilisé pour les modifieurs de la classe `Point` est le même
que pour la partie précédente :

1. Modifier l'objet et notifier les électeurs.
1. Si un électeur a refusé la modification :
   1. Remettre l'objet dans l'état initial
   1. Notifier les électeurs du retour à l'état initial

On suppose que l'état initial de l'objet de type `Point` a été accepté
par tous les électeurs.

Testez votre programme, notamment les tests unitaires doivent passer
et le programme `Test` doit fonctionner correctement.

N'oubliez pas de *committer* votre travail et de le synchroniser sur `Bitbucket`.

# Ecrire les méthode de la classe `StrategieMajoritaire` (2 points)

La stratégie majoritaire valide la modification si et seulement si le nombre de votes
**POUR** est supérieur au nombre de votes **CONTRE**.

Vous devez écrire le contenu des trois méthodes en utilisant le squelette
déjà fourni de la classe `StrategieMajoritaire`.

Vous pouvez vérifier avec le programme `Test`, que les modifications se comportent
comme souhaitées.

N'oubliez pas de *committer* votre travail après chaque modification avec un message
explicite.

# Ajouter une classe de test  `StrategieMajoritaireTest` (2 points)

La classe de test devra avoir au moins deux méthodes de test, une montrant
que le vote majoritaire valide la modification quand il y a une majorité
de **POUR** et une qui montre que la modification est refusée quand il y a
une majorité de **CONTRE**.

- - -

# Épilogue : enregistrement final

N'oubliez pas de *committer* votre travail après chaque modification avec un message
explicite.

Synchronisez votre travail avec le serveur Bitbucket par la commande :

```bash
git push --all
```

- - -
